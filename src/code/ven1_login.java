
package code;
/* -------------------------------------- Inicia Importacion librerias -------------------------------------- */
import AppPackage.AnimationClass;
import java.awt.Desktop;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URI;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
/* -------------------------------------- Finaliza Importacion librerias -------------------------------------- */
/**
 *
 * @author luise
 */
public class ven1_login extends javax.swing.JFrame {

    /**
     * Creates new form ven1_login
     */
    public ven1_login() {
        initComponents();
        //METODO COLOCAR TITULO A LA VENTANA
        /*01*/this.setTitle("Login");        
    }
    
    /* +++++++++++++++++++++++++++++++++++++ Inicia DECLARACION DE METODOS +++++++++++++++++++++++++++++++++++++ */
    //METODO 01 ICONO BARRA DE TAREAS
    @Override
    public Image getIconImage(){
    Image retValue = Toolkit.getDefaultToolkit().getImage(ClassLoader.getSystemResource("img_icon/ico_help.png"));
    return retValue;       
    }  
    
    //METODO 02 ICONO BARRA DE TAREAS
    private void met_02_icon_internet(){
        try {
            Desktop.getDesktop().browse(URI.create("https://www.facebook.com/6c7569736564756172646f"));
        } catch (IOException error_01) {
            /*VENTANA DE ALERTA EN CASO DE ERROR*/    
            Icon halo_v1 = new ImageIcon(getClass().getResource("/img_icon/Ico_bd_error.png"));
            JOptionPane.showMessageDialog(null, "¡Erro en el algoritmo!",
            " Atención",JOptionPane.YES_NO_CANCEL_OPTION,
            halo_v1);
            System.out.println("│──────────────────────────────────────────────────────────────────│");
            System.out.println("    Error en el método ► [met_02_icon_internet()] Línea ► [39] "+error_01);
            System.out.println("│──────────────────────────────────────────────────────────────────│");            
        }
    }
    /* +++++++++++++++++++++++++++++++++++++ Cierra DECLARACION DE METODOS +++++++++++++++++++++++++++++++++++++ */    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel_ingreso = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_usuario = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        txt_contraseña = new javax.swing.JPasswordField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel_menu = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel_internet = new javax.swing.JLabel();
        jLabel_calculadora = new javax.swing.JLabel();
        jLabel_musica = new javax.swing.JLabel();
        jPanel_title = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jPanel_header = new javax.swing.JPanel();
        jLabel_cronogramas = new javax.swing.JLabel();
        jLabel_socios = new javax.swing.JLabel();
        jLabel_drive = new javax.swing.JLabel();
        jLabel_notas = new javax.swing.JLabel();
        jLabel_mantenimiento = new javax.swing.JLabel();
        jLabel_programaciones = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setIconImage(getIconImage());
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel_ingreso.setBackground(new java.awt.Color(255, 255, 255));
        jPanel_ingreso.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)), "", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION));
        jPanel_ingreso.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel1.setText("USUARIO");
        jPanel_ingreso.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 193, 184, -1));

        txt_usuario.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt_usuario.setText("Ingrese usuario");
        txt_usuario.setBorder(null);
        txt_usuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_usuarioMouseClicked(evt);
            }
        });
        txt_usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_usuarioKeyPressed(evt);
            }
        });
        jPanel_ingreso.add(txt_usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 216, 184, 32));

        jSeparator1.setForeground(new java.awt.Color(0, 204, 255));
        jSeparator1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel_ingreso.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 248, 220, 15));

        jLabel2.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        jLabel2.setText("CONTRASEÑA");
        jPanel_ingreso.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 281, 220, -1));

        jSeparator2.setForeground(new java.awt.Color(0, 204, 255));
        jSeparator2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jPanel_ingreso.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 336, 220, 15));

        txt_contraseña.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt_contraseña.setText("Ingrese contraseña");
        txt_contraseña.setBorder(null);
        txt_contraseña.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_contraseñaMouseClicked(evt);
            }
        });
        jPanel_ingreso.add(txt_contraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 304, 184, 32));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_User_96px_2.png"))); // NOI18N
        jPanel_ingreso.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(89, 70, -1, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_customer_32px_1.png"))); // NOI18N
        jPanel_ingreso.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 216, -1, -1));

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Key_32px.png"))); // NOI18N
        jPanel_ingreso.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(34, 304, -1, -1));

        jLabel_menu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Menu_32px.png"))); // NOI18N
        jLabel_menu.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel_menu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel_menuMouseClicked(evt);
            }
        });
        jPanel_ingreso.add(jLabel_menu, new org.netbeans.lib.awtextra.AbsoluteConstraints(11, 12, -1, -1));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/Enter_OFF.png"))); // NOI18N
        jButton1.setBorder(null);
        jButton1.setBorderPainted(false);
        jButton1.setContentAreaFilled(false);
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/Enter_ON.png"))); // NOI18N
        jButton1.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/Enter_ON.png"))); // NOI18N
        jButton1.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/Enter_ON.png"))); // NOI18N
        jPanel_ingreso.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(69, 362, -1, -1));

        jLabel_internet.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Globe_32px.png"))); // NOI18N
        jLabel_internet.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel_internet.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel_internetMouseClicked(evt);
            }
        });
        jPanel_ingreso.add(jLabel_internet, new org.netbeans.lib.awtextra.AbsoluteConstraints(-40, 55, -1, -1));

        jLabel_calculadora.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Calculator_32px.png"))); // NOI18N
        jLabel_calculadora.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel_calculadora.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel_calculadoraMouseClicked(evt);
            }
        });
        jPanel_ingreso.add(jLabel_calculadora, new org.netbeans.lib.awtextra.AbsoluteConstraints(-40, 93, -1, -1));

        jLabel_musica.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Musical_Notes_32px.png"))); // NOI18N
        jLabel_musica.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jPanel_ingreso.add(jLabel_musica, new org.netbeans.lib.awtextra.AbsoluteConstraints(-40, 131, -1, -1));

        getContentPane().add(jPanel_ingreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 290, 580));

        jPanel_title.setBackground(new java.awt.Color(204, 204, 204));

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Search_32px_2.png"))); // NOI18N

        jTextField2.setFont(new java.awt.Font("Segoe UI", 0, 15)); // NOI18N
        jTextField2.setText("Buscar");

        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Expand_Arrow_32px.png"))); // NOI18N
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel19MouseClicked(evt);
            }
        });

        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Multiply_32px.png"))); // NOI18N
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel_titleLayout = new javax.swing.GroupLayout(jPanel_title);
        jPanel_title.setLayout(jPanel_titleLayout);
        jPanel_titleLayout.setHorizontalGroup(
            jPanel_titleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_titleLayout.createSequentialGroup()
                .addGap(296, 296, 296)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 144, Short.MAX_VALUE)
                .addComponent(jLabel19)
                .addGap(4, 4, 4)
                .addComponent(jLabel20)
                .addContainerGap())
        );
        jPanel_titleLayout.setVerticalGroup(
            jPanel_titleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_titleLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel_titleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField2))
                .addContainerGap())
            .addGroup(jPanel_titleLayout.createSequentialGroup()
                .addGroup(jPanel_titleLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel_title, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 0, 760, 50));

        jPanel_header.setBackground(new java.awt.Color(255, 255, 255));

        jLabel_cronogramas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_cronogramas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Clock_96px.png"))); // NOI18N
        jLabel_cronogramas.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel_cronogramasMouseMoved(evt);
            }
        });
        jLabel_cronogramas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel_cronogramasMouseExited(evt);
            }
        });

        jLabel_socios.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_socios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Handshake_96px.png"))); // NOI18N
        jLabel_socios.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel_sociosMouseMoved(evt);
            }
        });
        jLabel_socios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel_sociosMouseExited(evt);
            }
        });

        jLabel_drive.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_drive.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Google_Drive_96px.png"))); // NOI18N
        jLabel_drive.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel_driveMouseMoved(evt);
            }
        });
        jLabel_drive.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel_driveMouseExited(evt);
            }
        });

        jLabel_notas.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_notas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Idea_96px.png"))); // NOI18N
        jLabel_notas.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel_notasMouseMoved(evt);
            }
        });
        jLabel_notas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel_notasMouseExited(evt);
            }
        });

        jLabel_mantenimiento.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_mantenimiento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Maintenance_96px.png"))); // NOI18N
        jLabel_mantenimiento.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel_mantenimientoMouseMoved(evt);
            }
        });
        jLabel_mantenimiento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel_mantenimientoMouseExited(evt);
            }
        });

        jLabel_programaciones.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel_programaciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img_icon/icons8_Today_96px.png"))); // NOI18N
        jLabel_programaciones.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jLabel_programacionesMouseMoved(evt);
            }
        });
        jLabel_programaciones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel_programacionesMouseExited(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Cronogramas");

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Drive");

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Socios");

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Notas");

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("Mantenimiento");

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("Programaciones");

        javax.swing.GroupLayout jPanel_headerLayout = new javax.swing.GroupLayout(jPanel_header);
        jPanel_header.setLayout(jPanel_headerLayout);
        jPanel_headerLayout.setHorizontalGroup(
            jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_headerLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_headerLayout.createSequentialGroup()
                        .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel_cronogramas, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
                        .addGap(85, 85, 85)
                        .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel_drive, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                        .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel_socios, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel_headerLayout.createSequentialGroup()
                        .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel_notas, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
                        .addGap(85, 85, 85)
                        .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel_mantenimiento, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 91, Short.MAX_VALUE)
                        .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel_programaciones, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(28, 28, 28))
        );
        jPanel_headerLayout.setVerticalGroup(
            jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel_headerLayout.createSequentialGroup()
                .addGap(94, 94, 94)
                .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel_drive, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_cronogramas, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_socios, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(26, 26, 26)
                .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel_mantenimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_notas, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel_programaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel_headerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17))
                .addContainerGap(68, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel_header, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 50, 760, 530));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel_menuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_menuMouseClicked
    //Code Mostrar
    AnimationClass ico_internet_mostrar = new AnimationClass();
    ico_internet_mostrar.jLabelXRight(-40, 10, 10, 5, jLabel_internet);
    //--------------------------------------------------------------------------
    AnimationClass ico_calculadora_mostrar = new AnimationClass();
    ico_calculadora_mostrar.jLabelXRight(-40, 10, 10, 5, jLabel_calculadora);
    //--------------------------------------------------------------------------
    AnimationClass ico_musica_mostrar = new AnimationClass();
    ico_musica_mostrar.jLabelXRight(-40, 10, 10, 5, jLabel_musica);
    
    //Code Ocultar
    AnimationClass ico_internet_ocutar = new AnimationClass();
    ico_internet_ocutar.jLabelXLeft(10, -40, 10, 5, jLabel_internet);
    //--------------------------------------------------------------------------
    AnimationClass ico_calculadora_ocutar = new AnimationClass();
    ico_calculadora_ocutar.jLabelXLeft(10, -40, 10, 5, jLabel_calculadora);
    //--------------------------------------------------------------------------
    AnimationClass ico_musica_ocutar = new AnimationClass();
    ico_musica_ocutar.jLabelXLeft(10, -40, 10, 5, jLabel_musica);
    //--------------------------------------------------------------------------
    }//GEN-LAST:event_jLabel_menuMouseClicked

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
    //Code boton cerrar
    int dialog = JOptionPane.YES_NO_OPTION;
    int result = JOptionPane.showConfirmDialog(rootPane, "¿Desea Salir?", "Exit", dialog);
    if(result == 0){System.exit(0);}
    }//GEN-LAST:event_jLabel20MouseClicked

    private void jLabel19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseClicked
    //Code boton minimizar
    this.setState(ven1_login.ICONIFIED);
    }//GEN-LAST:event_jLabel19MouseClicked

    private void jLabel_cronogramasMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_cronogramasMouseMoved
    //Code mostrar border
    jLabel_cronogramas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    }//GEN-LAST:event_jLabel_cronogramasMouseMoved

    private void jLabel_cronogramasMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_cronogramasMouseExited
    //Code ocultar border
    jLabel_cronogramas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    }//GEN-LAST:event_jLabel_cronogramasMouseExited

    private void jLabel_driveMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_driveMouseMoved
    //Code mostrar border
    jLabel_drive.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    }//GEN-LAST:event_jLabel_driveMouseMoved

    private void jLabel_driveMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_driveMouseExited
    //Code ocultar border
    jLabel_drive.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    }//GEN-LAST:event_jLabel_driveMouseExited

    private void jLabel_sociosMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_sociosMouseMoved
    //Code mostrar border
    jLabel_socios.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    }//GEN-LAST:event_jLabel_sociosMouseMoved

    private void jLabel_sociosMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_sociosMouseExited
    //Code mostrar border
    jLabel_socios.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    }//GEN-LAST:event_jLabel_sociosMouseExited

    private void jLabel_notasMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_notasMouseMoved
    //Code mostrar border
    jLabel_notas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
    }//GEN-LAST:event_jLabel_notasMouseMoved

    private void jLabel_notasMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_notasMouseExited
    //Code ocultar border
    jLabel_notas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    }//GEN-LAST:event_jLabel_notasMouseExited

    private void jLabel_mantenimientoMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_mantenimientoMouseMoved
    //Code mostrar border
    jLabel_mantenimiento.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));    
    }//GEN-LAST:event_jLabel_mantenimientoMouseMoved

    private void jLabel_mantenimientoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_mantenimientoMouseExited
    //Code ocultar border
    jLabel_mantenimiento.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    }//GEN-LAST:event_jLabel_mantenimientoMouseExited

    private void jLabel_programacionesMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_programacionesMouseMoved
    //Code mostrar border
    jLabel_programaciones.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));    
    }//GEN-LAST:event_jLabel_programacionesMouseMoved

    private void jLabel_programacionesMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_programacionesMouseExited
    //Code ocultar border
    jLabel_programaciones.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
    }//GEN-LAST:event_jLabel_programacionesMouseExited

    private void txt_usuarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_usuarioKeyPressed
    /*NULL*/
    }//GEN-LAST:event_txt_usuarioKeyPressed

    private void txt_usuarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_usuarioMouseClicked
    //Code Vaciar campo de texto
    txt_usuario.setText("");
    }//GEN-LAST:event_txt_usuarioMouseClicked

    private void txt_contraseñaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_contraseñaMouseClicked
    //Code Vaciar campo de texto
    txt_contraseña.setText("");
    }//GEN-LAST:event_txt_contraseñaMouseClicked

    private void jLabel_internetMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_internetMouseClicked
    //Llamado de metodo icono internet
    met_02_icon_internet();
    }//GEN-LAST:event_jLabel_internetMouseClicked

    private void jLabel_calculadoraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel_calculadoraMouseClicked
    /*Code llamar a la calculadora*/
        try {
        Runtime rt= Runtime.getRuntime();
        Process p = rt.exec("calc");
        p.waitFor();
    } catch (IOException | InterruptedException error_02) {
            /*VENTANA DE ALERTA EN CASO DE ERROR*/    
            Icon halo_v1 = new ImageIcon(getClass().getResource("/img_icon/Ico_bd_error.png"));
            JOptionPane.showMessageDialog(null, "¡Erro en el algoritmo!",
            " Atención",JOptionPane.YES_NO_CANCEL_OPTION,
            halo_v1);
            System.out.println("│──────────────────────────────────────────────────────────────────│");
            System.out.println("    Error en el método ► [jLabel_calculadoraMouseClicked] Línea ► [529] "+error_02);
            System.out.println("│──────────────────────────────────────────────────────────────────│");          
    }
    }//GEN-LAST:event_jLabel_calculadoraMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ven1_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ven1_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ven1_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ven1_login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ven1_login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel_calculadora;
    private javax.swing.JLabel jLabel_cronogramas;
    private javax.swing.JLabel jLabel_drive;
    private javax.swing.JLabel jLabel_internet;
    private javax.swing.JLabel jLabel_mantenimiento;
    private javax.swing.JLabel jLabel_menu;
    private javax.swing.JLabel jLabel_musica;
    private javax.swing.JLabel jLabel_notas;
    private javax.swing.JLabel jLabel_programaciones;
    private javax.swing.JLabel jLabel_socios;
    private javax.swing.JPanel jPanel_header;
    private javax.swing.JPanel jPanel_ingreso;
    private javax.swing.JPanel jPanel_title;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JPasswordField txt_contraseña;
    private javax.swing.JTextField txt_usuario;
    // End of variables declaration//GEN-END:variables
}
